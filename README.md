# megadrive

based on https://archive.org/details/no-intro_romsets (20210331-072311)


ROM not added :
```sh
❯ tree -hs --dirsfirst
.
├── [1.1M]  Alien Soldier (USA) (Virtual Console).zip
├── [242K]  Alter Ego (World) (Aftermarket) (Unl).zip
├── [259K]  Arkagis Revolution (World) (En,Es) (Prerelease) (2019-07-29) (Unl).zip
├── [260K]  Arkagis Revolution (World) (En,Es) (Prerelease) (2019-09-02) (Unl).zip
├── [260K]  Arkagis Revolution (World) (En,Es) (Prerelease) (2019-09-16) (Unl).zip
├── [264K]  Arkagis Revolution (World) (En,Fr,Es) (Prerelease) (2019-02-21) (Unl).zip
├── [264K]  Arkagis Revolution (World) (En,Fr,Es) (Prerelease) (2019-11-28) (Unl).zip
├── [264K]  Arkagis Revolution (World) (En,Fr,Es) (Prerelease) (2020-03-04) (Unl).zip
├── [927K]  Arkagis Revolution (World) (En,Fr,Es) (Rev 1) (Aftermarket) (Unl).zip
├── [ 79K]  Bio Evil (World) (Demo) (Aftermarket) (Unl).zip
├── [ 86K]  [BIOS] Aiwa CSD-GM1 (Japan).zip
├── [ 87K]  [BIOS] LaserActive (Japan) (v1.02).zip
├── [ 81K]  [BIOS] LaserActive (Japan) (v1.05).zip
├── [ 88K]  [BIOS] LaserActive (USA) (v1.02).zip
├── [ 88K]  [BIOS] LaserActive (USA) (v1.04).zip
├── [ 90K]  [BIOS] Mega-CD 2 (Europe) (Rev A).zip
├── [ 90K]  [BIOS] Mega-CD 2 (Europe).zip
├── [ 85K]  [BIOS] Mega-CD 2 (Japan).zip
├── [ 85K]  [BIOS] Mega-CD (Asia) (Ja) (Rev H).zip
├── [ 87K]  [BIOS] Mega-CD (Europe).zip
├── [ 84K]  [BIOS] Mega-CD (Japan) (Beta).zip
├── [ 84K]  [BIOS] Mega-CD (Japan) (Rev A).zip
├── [ 84K]  [BIOS] Mega-CD (Japan) (Rev B).zip
├── [ 85K]  [BIOS] Mega-CD (Japan) (Rev C).zip
├── [ 85K]  [BIOS] Mega-CD (Japan) (Rev D).zip
├── [ 85K]  [BIOS] Mega-CD (Japan) (Rev E).zip
├── [ 85K]  [BIOS] Mega-CD (Japan) (Rev H).zip
├── [ 85K]  [BIOS] Mega-CD (Japan) (v1.11B).zip
├── [ 90K]  [BIOS] Multi-Mega (Europe).zip
├── [ 92K]  [BIOS] Sega CD 2A (USA).zip
├── [ 91K]  [BIOS] Sega CD 2 (USA) (Rev A).zip
├── [ 91K]  [BIOS] Sega CD 2 (USA).zip
├── [ 91K]  [BIOS] Sega CD (USA) (Rev B).zip
├── [ 91K]  [BIOS] Sega CD (USA) (v1.00).zip
├── [ 92K]  [BIOS] Sega CDX (USA).zip
├── [ 930]  [BIOS] Sega Mega Drive - Genesis Boot ROM (World).zip
├── [ 84K]  [BIOS] WonderMega (Japan) (En).zip
├── [ 86K]  [BIOS] WonderMega M2 (Japan).zip
├── [ 86K]  [BIOS] X'Eye (USA).zip
├── [182K]  Breach (USA) (Proto).zip
├── [ 44K]  CDX Pro (World) (v1.70) (Program) (Unl) [b].zip
├── [656K]  Clan of Heroes - Generals of the Yang Family (World) (Aftermarket) (Unl).zip
├── [1.5M]  Comix Zone (USA) (Virtual Console).zip
├── [260K]  Curse of Illmoore Bay, The (World) (Demo 1) (Aftermarket) (Unl).zip
├── [411K]  Curse of Illmoore Bay, The (World) (Demo 2) (Aftermarket) (Unl).zip
├── [488K]  Curse of Illmoore Bay, The (World) (Demo 3) (Aftermarket) (Unl).zip
├── [184K]  Curse (USA) (Proto) (1990-06-26) (Sega Channel).zip
├── [364K]  Custodian (World) (Aftermarket) (Unl).zip
├── [501K]  Demons of Asteborg (World) (Demo 1) (Aftermarket) (Unl).zip
├── [1.3M]  Demons of Asteborg (World) (Demo 2) (Aftermarket) (Unl).zip
├── [1.4M]  Dyna Brothers 2 Special (Japan) (Virtual Console).zip
├── [ 76K]  Elite (Unknown) (Tech Demo, Game).zip
├── [ 74K]  Elite (Unknown) (Tech Demo, Soaker).zip
├── [ 75K]  Elite (Unknown) (Tech Demo, Viewer).zip
├── [1.1M]  FIFA Soccer 2000 (Russia) (En,Fr,De,Es,It,Sv) (Unl) (Pirate).zip
├── [1.1M]  Futbol Argentino 96 (Argentina) (Unl) (Pirate).zip
├── [1.1M]  Futbol Argentino 98 - Pasion de Multitudes (Argentina) (Unl) (Pirate).zip
├── [1.1M]  Futbol Argentino (Argentina) (Unl) (Pirate).zip
├── [1.4M]  FX Unit Yuki - The Henshin Engine (World) (Aftermarket) (Unl).zip
├── [423K]  Galaxy Force II (Japan) (En) (Virtual Console).zip
├── [668K]  Golden Axe III (Europe) (Virtual Console).zip
├── [324K]  Home Alone (USA, Europe) (Beta) (July, 1992).zip
├── [757K]  I Love Mickey and Donald - Fushigi na Magic Box (Japan) (Mega Drive Mini).zip
├── [608K]  Instruments of Chaos Starring Young Indiana Jones (USA) (Beta) (1994-01-01).zip
├── [608K]  Instruments of Chaos Starring Young Indiana Jones (USA) (Beta) (1994-01-03).zip
├── [607K]  Instruments of Chaos Starring Young Indiana Jones (USA) (Beta) (1994-01-26).zip
├── [607K]  Instruments of Chaos Starring Young Indiana Jones (USA) (Beta) (1994-01-27).zip
├── [663K]  Instruments of Chaos Starring Young Indiana Jones (USA) (Beta) (July, 1992).zip
├── [266K]  Juusou Kihei Leynos (Japan) (Virtual Console).zip
├── [359K]  Mega 3D Noah's Ark (Unknown) (Aftermarket) (Unl).zip
├── [639K]  Mega Cheril Perils (World) (En,Es) (Aftermarket) (Unl).zip
├── [145K]  MegaNet (Brazil) (Program).zip
├── [576K]  Mega Turrican (USA, Europe) (Virtual Console).zip
├── [490K]  Metal Blast 2277 (World) (Beta) (Aftermarket) (Unl).zip
├── [511K]  Metal Blast 2277 (World) (Rev 1) (Aftermarket) (Unl).zip
├── [101K]  Miniplanets (World) (Rev 2) (Aftermarket) (Unl).zip
├── [102K]  Miniplanets (World) (Rev 3) (Aftermarket) (Unl).zip
├── [254K]  Misplaced (Russia) (Aftermarket).zip
├── [254K]  Misplaced (World) (Aftermarket) (Unl).zip
├── [982K]  Monster World IV (USA, Europe) (En,Ja) (Virtual Console).zip
├── [1.1M]  Nuclear Rush (USA) (Proto) (1994-08-06).zip
├── [470K]  Phantasy Star II (USA, Europe) (Rev A) (Virtual Console).zip
├── [254K]  Phelios (Japan) (Virtual Console).zip
├── [1010K]  Puyo Puyo Tsuu (World) (Ja) (Rev A) (Virtual Console).zip
├── [359K]  Revenge of Shinobi, The (World) (Rev B) (Virtual Console).zip
├── [3.3M]  Sonic & Knuckles + Sonic The Hedgehog 2 + Sonic The Hedgehog 3 (World) (Virtual Console).zip
├── [732K]  Sonic The Hedgehog 2 (USA, Europe) (Rev A) (Virtual Console).zip
├── [376K]  Sonic The Hedgehog (Japan) (En) (Virtual Console).zip
├── [417K]  Sorcerian (Japan) (Rev 1) (Virtual Console).zip
├── [1.5M]  Street Fighter II' Plus - Champion Edition (Japan) (Virtual Console).zip
├── [1.5M]  Street Fighter II' - Special Champion Edition (Europe) (Virtual Console).zip
├── [1.5M]  Street Fighter II' - Special Champion Edition (USA) (Virtual Console).zip
├── [669K]  Strider Hiryuu (Japan) (Rev A) (Virtual Console).zip
├── [670K]  Strider (USA, Europe) (Virtual Console).zip
├── [2.7M]  Super Street Fighter II (Europe) (Virtual Console).zip
├── [2.7M]  Super Street Fighter II - The New Challengers (Japan) (Virtual Console).zip
├── [2.7M]  Super Street Fighter II (USA) (Virtual Console).zip
├── [398K]  Switchblade (World) (Aftermarket) (Unl).zip
├── [333K]  Tecmo Cup Football Game (Japan) (Proto) [b].zip
├── [186K]  Wonder Library (Japan) (Program).zip
└── [177K]  Young Indiana Jones Chronicles, The (USA) (Beta).zip

0 directories, 101 files
```